#include <ctime>
#include <iostream>
#include "loguru.hpp"
#include <sys/stat.h>
#include "loguruzip.h"

Loguruzip::Loguruzip()
{

}

void Loguruzip::start(int argc , char **argv)
{
    g_argc=argc;
    g_argv=argv;
    status  = started;
    init();
    thr1=std::thread(&Loguruzip::checkLogSize,this);
    thr2=std::thread(&Loguruzip::setLogData,this);
}

void Loguruzip::init()
{
    loguru::g_stderr_verbosity=0;
    loguru::add_file(filename.c_str(), loguru::Append, loguru::Verbosity_MAX);
    if(add_file(filename.c_str(), loguru::Append, loguru::Verbosity_MAX)==false) {
        std::cout<<"Error: file creation failed"<<std::endl;
    }
    else {
        std::cout<<"File creation succesful"<<std::endl;
    }
    loguru::init(g_argc,g_argv);
}

void Loguruzip::setLogData()
{
    while(status == started){
        LOG_S(2) << "Look at my custom object: " ;
    }
}

void Loguruzip::checkLogSize()
{
    while (status == started) {
        if(getFileSize()>1*1024*1024*1024){
            loguru::shutdown();
            std::cout<<"making zip..."<<std::endl;
            int curr_val=60;
            char command[256];
            std::string comm = "zip -r "+ currentDateTime()+ ".zip "+filename+".log";
            snprintf(command, 256, comm.c_str(), curr_val);
            system(command);
            std::cout<<"zip file made"<<std::endl;
            std::remove(filename.c_str());
            init();
        }
    }
}

long Loguruzip::getFileSize()
{
    struct stat stat_buf;
    int rc = stat(filename.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

void Loguruzip::stop()
{
    status = none_started;
    loguru::shutdown();
    thr1.join();
    thr2.join();
}

std::string Loguruzip::currentDateTime()
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    return buf;
}

