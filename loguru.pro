TEMPLATE = app

CONFIG += console c++14
CONFIG -= app_bundle - qt

QT -= gui core

LIBS += -lpthread -ldl -lgtest -lgtest_main

SOURCES += \
        loguru.cpp \
        loguruzip.cpp \
        main.cpp \
        testlogzip.cpp

HEADERS += \
    loguru.hpp \
    loguruzip.h \
    testlogzip.h



