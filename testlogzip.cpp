#include "loguru.hpp"
#include "loguruzip.h"
#include "testlogzip.h"

int g_argc;
char **g_argv;

TEST(filename_test, get_filename)
{
    Loguruzip obj;
    std::string name=obj.filename;

    ASSERT_EQ("everything.log", name);
}
TEST(filename_test,change_filename)
{
    Loguruzip obj;
    std::string name="test.log";
    obj.filename=name;

    ASSERT_EQ("test.log",obj.filename);
}
TEST(filesize_test,get_file_size)
{
    Loguruzip obj;
    obj.filename="filetest.log";
    obj.start(g_argc,g_argv);
    obj.stop();

    ASSERT_LT(1,obj.getFileSize());
    ASSERT_NE(0,obj.getFileSize());
}

TEST(Start_test,check_file_size)
{
    Loguruzip obj;
    obj.filename="starttest.log";
    obj.start(g_argc,g_argv);
    long firstfilesize=obj.getFileSize();
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));
    obj.stop();

    ASSERT_NE(firstfilesize,obj.getFileSize());
    ASSERT_GT(obj.getFileSize(),firstfilesize);
}

TEST(Stop_test,chech_last_file_size)
{
    Loguruzip obj;
    obj.filename="stoptest.log";
    obj.start(g_argc,g_argv);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));
    obj.stop();
    long lastfilesize=obj.getFileSize();
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    ASSERT_TRUE(lastfilesize==obj.getFileSize());
}

Testlogzip::Testlogzip()
{

}

int Testlogzip::startmain(int argc ,char *argv[]){
    g_argc=argc;
    g_argv=argv;
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
