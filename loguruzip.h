#ifndef LOGURUZIP_H
#define LOGURUZIP_H

#include <thread>
#include <iostream>

class Loguruzip
{
public:
    Loguruzip();
    void stop();
    long getFileSize();
    std::string filename="everything.log";
    virtual void start(int argc , char **argv);

protected:
    void init();
    void setLogData();
    void checkLogSize();
    std::string currentDateTime();
    enum state {none_started,started};
private:
    int g_argc;
    char **g_argv;
    std::thread thr1;
    std::thread thr2;
    state status = none_started;
};

#endif // LOGURUZIP_H
